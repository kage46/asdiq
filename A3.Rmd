---
title: "Assignment3"
author: "RF"
date: "3/6/2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(rethinking)
library(brms)
library(readr)
library(tidyverse)
setwd("~/Cognitive Science 4th semester/Computational modeling for cognitive science/portfolio assignments/asdiq")
data <- read_csv("Assignment3Data.csv")
#ds <- read_csv("~/Desktop/MAC/Computational Modelling/R. Bayesian/Assignment 3/Assignment3Data.csv")
#data <- ds
#data <- read.csv("Assignment3Data.csv")
```

## Multivariate linear models

In this assignment you will investigate the relation between different aspects of IQ and symptom severity in ASD (as measured by ADOS). 
The assignment is meant to make you practice on linear models, multiple predictors and interactions.

The data you will use is the outcome of the psychological testing of the children you analyzed in methods 3.
Data: https://www.dropbox.com/s/hom2qnmp9hyfwmu/Assignment3Data.csv?dl=0
The data consists of ChildID, gender, age, diagnosis (ASD), symptom severity (ADOS), several aspects of IQ (NonVerbal/Perceptual,Verbal,Social) and the visit at which the tests were administered. The questions should be answered by focusing on visit 1.

The questions you will have to answer are:

1. Assess the relation between symptom severity and IQ (focus on visit 1 and children with ASD) and report the model, a plot of the model, a couple of lines describing the quality of the model and interpreting the results. 
P.S. Should you scale? 
P.P.S. Don't forget to motivate your priors. 
P.P.P.S. At least one plot for results and a plot for quality of each model (here and in the next questions) would be appreciated.

1.1. Verbal IQ and ADOS
1.2. Non Verbal IQ and ADOS
1.3. Social IQ and ADOS

```{r}
data2 <- data[!is.na(data$ADOS),]
data2 <- data.frame(filter(data2, Visit == 1 & ASD == 1))


dens(data2$ADOS)
data2$ADOS.s <- scale(data2$ADOS)
dens(data2$ADOS.s)
```

```{r}

VerbalADOS <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b * VerbalIQ ,
    a ~ dnorm(13 , 10) , #mean(data2$ADOS)
    b ~ dnorm(0 , 5),
    sigma ~ dunif( 0 , 10 )
    ), data=data2)

precis(VerbalADOS)
#        Mean StdDev  5.5% 94.5%
# a     21.62   1.40 19.38 23.85
# b     -0.43   0.07 -0.55 -0.32
# sigma  3.16   0.38  2.55  3.78
```

```{r}
VerbalADOS2 <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b * VerbalIQ ,
    a ~ dnorm(21 , 5) , #mean(data2$ADOS)
    b ~ dnorm(0 , 0.1),
    sigma ~ dunif( 0 , 10 ) 
), data=data2)

precis(VerbalADOS2)
#        Mean StdDev  5.5% 94.5%
# a     18.94   1.28 16.89 20.99
# b     -0.28   0.06 -0.39 -0.18
# sigma  3.38   0.45  2.67  4.10
```

```{r}
plot(ADOS ~ VerbalIQ, data = data2)
abline( a=coef(VerbalADOS)["a"] , b=coef(VerbalADOS)["b"] )

plot(ADOS ~ VerbalIQ, data = data2)
abline( a=coef(VerbalADOS2)["a"] , b=coef(VerbalADOS2)["b"] )

#define priors for predictive check 
prior = c(prior(normal(13, 10), class = "Intercept"), 
          prior(normal(0, 10), class = "sigma"), 
          prior(normal(0,5), class = "b"))


#predictive posterior plot 
VERBAL <- brm(ADOS ~ VerbalIQ, prior = prior, data2) # no priors specified, standard priors 

# Posterior Predictive Check
pp_check(VERBAL)
```


#non-verbal 
```{r}
NONVerbalADOS <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b * NonVerbalIQ ,
    a ~ dnorm(21 , 5) , #mean(data2$ADOS)
    b ~ dnorm(0 , 0.1),
    sigma ~ dunif( 0 , 10 ) 
), data=data2)

precis(NONVerbalADOS)
#        Mean StdDev  5.5% 94.5%
# a     19.15   2.15 15.72 22.58
# b     -0.20   0.08 -0.32 -0.07
# sigma  4.07   0.52  3.24  4.89

plot(ADOS ~ NonVerbalIQ, data = data2)
abline( a=coef(NONVerbalADOS)["a"] , b=coef(NONVerbalADOS)["b"] )

#define priors for predictive check 
prior = c(prior(normal(13, 10), class = "Intercept"), 
          prior(normal(0, 10), class = "sigma"), 
          prior(normal(0,5), class = "b"))


#predictive posterior plot 
NONVERBAL <- brm(ADOS ~ NonVerbalIQ, prior = prior, data2) # no priors specified, standard priors 

# Posterior Predictive Check
pp_check(NONVERBAL)

```


#social 
```{r}
SocialADOS <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b * SocialIQ ,
    a ~ dnorm(13 , 10) , #mean(data2$ADOS) intercept 
    b ~ dnorm(0 , 10), #slope 
    sigma ~ dunif( 0 , 10) #standard dev 
), data=data2)

precis(SocialADOS) #sig 10 
#        Mean StdDev  5.5% 94.5%
# a     31.98   3.89 25.76 38.20
# b     -0.23   0.05 -0.31 -0.15
# sigma  3.43   0.42  2.75  4.11


plot(ADOS ~ SocialIQ, data = data2)
abline( a=coef(SocialADOS)["a"] , b=coef(SocialADOS)["b"] )

#define priors for predictive check 
prior = c(prior(normal(13, 10), class = "Intercept"), 
          prior(normal(0, 10), class = "sigma"), 
          prior(normal(0,5), class = "b"))


#predictive posterior plot 
SOCIAL <- brm(ADOS ~ SocialIQ, prior = prior, data2) # no priors specified, standard priors 

# Posterior Predictive Check
pp_check(SOCIAL)
```

```{r}
#scaling data
dens(data2$ADOS)
data2$ADOS.s <- scale(data2$ADOS)
data2$VerbalIQ.s <- scale(data2$VerbalIQ)
data2$NonVerbalIQ.s <- scale(data2$NonVerbalIQ)
data2$SocialIQ.s <- scale(data2$SocialIQ)
dens(data2$ADOS.s)


```





2. Do the different aspects of IQ account for different portions of the variance in ADOS? 
2.1. Does it make sense to have all IQ measures in the same model? First write a few lines answering the question and motivating your answer, including a discussion as to what happens when you put all of them in the same model. Then build a model following your answer. If your answer is "no", you are not free, you still have to answer: are there alternative ways of answering the question?
2.2. Build the model, assess its quality, write a few lines interpreting the results.

```{r}
##### NOGET KODE CARO SENDTE

VerNonIQ.s <- rethinking::map( alist(
  ADOS.s ~ dnorm( mu , sigma ) ,
  mu <- a + b.ver*VerbalIQ.s + b.non*NonVerbalIQ.s, 
  a ~ dnorm( 13 , 10 ) ,
  b.ver ~ dnorm( 0 , 5 ) ,
  b.non ~ dnorm( 0 , 5) ,
  sigma ~ dunif( 0 , 10 )
  ),
  data=data2 )

verSOIQ.s <- rethinking::map( alist(
  ADOS.s ~ dnorm( mu , sigma ) ,
  mu <- a + b.ver*VerbalIQ.s + b.soc*SocialIQ.s, 
  a ~ dnorm( 13 , 10 ) ,
  b.ver ~ dnorm( 0 , 5 ) ,
  b.soc ~ dnorm( 0 , 5) ,
  sigma ~ dunif( 0 , 10 )
  ),
  data=data2 )

nonSOIQ.s <- rethinking::map( alist(
  ADOS.s ~ dnorm( mu , sigma ) ,
  mu <- a + b.non*NonVerbalIQ.s + b.soc*SocialIQ.s, 
  a ~ dnorm( 13 , 10 ) ,
  b.non ~ dnorm( 0 , 5 ) ,
  b.soc ~ dnorm( 0 , 5) ,
  sigma ~ dunif( 0 , 10 )
  ),
  data=data2 )

ALLIQ.s <- rethinking::map( alist(
  ADOS.s ~ dnorm( mu , sigma ) ,
  mu <- a + b.ver*VerbalIQ.s + b.non*NonVerbalIQ.s + b.soc*SocialIQ.s, 
  a ~ dnorm( 13 , 10 ) ,
  b.ver ~ dnorm( 0 , 5 ) ,
  b.non ~ dnorm( 0 , 5 ) ,
  b.soc ~ dnorm( 0 , 5) ,
  sigma ~ dunif( 0 , 10 )
  ),
  data=data2 )

#making a variance-covariance matrix, which shows correlation among parameters 
precis(ALLIQ.s, corr = TRUE)

#predictive posterior plot model
#ALLIQModel <- brm(ADOS.s ~ VerbalIQ.s + SocialIQ.s + NonVerbalIQ.s, prior = prior, data2) 

# Posterior Predictive Check
#pp_check(ALLIQModel)
#precis(verSOIQ.s, corr = T)


compare(VerNonIQ.s, verSOIQ.s, nonSOIQ.s, ALLIQ.s)
#             WAIC pWAIC dWAIC weight    SE  dSE
# verSOIQ.s   74.7   5.4   0.0   0.51  8.59   NA
# ALLIQ.s     75.4   6.6   0.7   0.36  9.75 3.54
# VerbalADOS2 77.9   3.7   3.1   0.11  8.01 6.21
# NonverIQ.s  81.1   6.1   6.4   0.02 11.34 9.46

#weight is the models probability to predict data - hight goood

```

```{r}
#DAG code
#working 
dag2 <- dagify(Diagnosis ~ VerIQ + NonVerIQ + SocIQ,
               VerIQ ~ Q1 + Q2 + Qn,
               NonVerIQ ~ Q1 + Q2 + Qn,
               SocIQ ~ Q1 + Q2 + Qn,
               Q1 ~ ASD,
               Q2 ~ ASD,
               Q1 ~ ASD,
               exposure = "x",
               outcome = "Diagnosis")

ggdag(dag2)
```



3. Let's now include also the TD children. Does it make sense to ask whether IQ and ADOS are related? Motivate your answer. In any case, if you wanted to build a model to answer that question, which model would you build? Run the model, assess its quality, write a few lines interpreting the results.

```{r}
#creating data including TD and ASD only from visit 1 
data_ALL <- data[ complete.cases(data), ]
data_ALL <- data.frame(filter(data_ALL, Visit == "1"))
data_ALL$ASD2 <- as.character(data_ALL$ASD)

#plotting all data differently
ggplot(data_ALL, aes(x=VerbalIQ, y=ADOS, color = ASD)) + 
  geom_point()+ 
  geom_smooth(method = lm)

ggplot(data_ALL, aes(x=VerbalIQ, y=ADOS, color = ASD2)) + 
  geom_point()+ 
  geom_smooth(method = lm)

#verbal IQ all
ALL_VerbalADOS <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b * VerbalIQ ,
    a ~ dnorm(13 , 10) , #mean(data2$ADOS)
    b ~ dnorm(0 , 5),
    sigma ~ dunif( 0 , 10 ) 
), data=data_ALL)

precis(ALL_VerbalADOS) 
#       Mean StdDev  5.5% 94.5%
#a     15.14   2.47 11.19 19.08
#b     -0.42   0.12 -0.62 -0.23
#sigma  6.80   0.57  5.88  7.72


```


4. Let's discuss contents:
4.1. You have three scores for IQ, do they show shared variance? Is that the same in TD and ASD? What does that tell us about IQ?
```{r}
#asd data
data_asd <- data[ complete.cases(data), ]
data_asd <- data.frame(filter(data_asd, ASD == 1))

data_asd$ADOS.s <- scale(data_asd$ADOS)
data_asd$VerbalIQ.s <- scale(data_asd$VerbalIQ)
data_asd$NonVerbalIQ.s <- scale(data_asd$NonVerbalIQ)
data_asd$SocialIQ.s <- scale(data_asd$SocialIQ)

#td data
data_td <- data[ complete.cases(data), ]
data_td <- data.frame(filter(data_td, ASD == 0))

data_td$ADOS.s <- scale(data_td$ADOS)
data_td$VerbalIQ.s <- scale(data_td$VerbalIQ)
data_td$NonVerbalIQ.s <- scale(data_td$NonVerbalIQ)
data_td$SocialIQ.s <- scale(data_td$SocialIQ)

#variance for IQ scores
#Lau
asd_social <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b.ver*VerbalIQ.s + b.non*NonVerbalIQ.s + b.soc*SocialIQ.s, 
    a ~ dnorm(13 , 10) , #mean(data2$ADOS) intercept 
    b.ver ~ dnorm( 0 , 5 ) ,
    b.non ~ dnorm( 0 , 5 ) ,
    b.soc ~ dnorm( 0 , 5) ,
    sigma ~ dunif( 0 , 10) #standard dev 
), data=data_asd)

td_social <- rethinking::map(
  alist(
    ADOS ~ dnorm( mu , sigma ) ,
    mu <- a + b.ver*VerbalIQ.s + b.non*NonVerbalIQ.s + b.soc*SocialIQ.s, 
    a ~ dnorm(13 , 10) , #mean(data2$ADOS) intercept 
    b.ver ~ dnorm( 0 , 5 ) ,
    b.non ~ dnorm( 0 , 5 ) ,
    b.soc ~ dnorm( 0 , 5) ,
    sigma ~ dunif( 0 , 10) #standard dev 
), data=data_td)

#plot(precis(asd_social))

tingDims <- precis(asd_social)
tingDimstd <- precis(td_social)
precis_plot(tingDims)
precis_plot(tingDimstd)

#Lau part 2
post <- extract.samples(asd_social)
plot( data_asd$SocialIQ ~ data_asd$VerbalIQ, post , col=col.alpha(rangi2,0.5) , pch=16 )
plot( data_asd$SocialIQ ~ data_asd$NonVerbalIQ, post , col=col.alpha(rangi2,0.5) , pch=16 )
plot( data_asd$NonVerbalIQ ~ data_asd$VerbalIQ, post , col=col.alpha(rangi2,0.5) , pch=16 )


```

4.2. You have explored the relation between IQ and ADOS. How do you explain that relation from a cognitive perspective? N.B. You can present alternative hypotheses.

5. Bonus questions: Including measurement errors. 
5.1. Let's assume that ADOS has a measurement error of 1. How would you model that in any of the previous models? 
5.2. We know that IQ has an estimated measurement error of 2.12. How would you include that? 
